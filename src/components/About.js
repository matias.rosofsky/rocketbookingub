import React from 'react';
import { arrayUs } from '../utils/Constants';
import PresentationCard from '../shared/PresentationCard';

const About = () => {
    return (
        <div className='images-div'>
            {arrayUs.map((person, i) => <PresentationCard key={i} person={person} />)}
        </div>
    )
}

export default About;