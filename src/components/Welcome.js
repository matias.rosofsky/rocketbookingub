import React from 'react';
import { Link } from 'react-router-dom';

export const Welcome = () => {
	return (
		<div className="divWelcome">
			<h1 className="mainTitle">Bienvenido a RocketBooking</h1>
		</div>
	)
}
export default Welcome
