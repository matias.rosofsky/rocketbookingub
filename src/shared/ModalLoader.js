import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';

function ModalLoader({ loading, content }) {
    return (
        <Dimmer active inverted>
            <Loader
                active={loading}
                content={content}
                size='massive'
                className='loader'
            />
        </Dimmer>
    )
}

export default ModalLoader;