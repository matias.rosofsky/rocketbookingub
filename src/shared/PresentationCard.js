import React from 'react';
import { Card, Image } from 'semantic-ui-react';
import { SEND_EMAIL_MESSAGE } from '../utils/Constants';

function PresentationCard({ person }) {
    return (
        <Card className='images-card'>
            <Image src={person.PHOTO} wrapped ui={false} />
            <Card.Content>
                <Card.Header>{person.NAME}</Card.Header>
                <Card.Meta>
                    <span className='date'>{person.AGE}</span>
                </Card.Meta>
                <Card.Description>
                    {person.WORK}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <p><a href={person.MAIL}>{SEND_EMAIL_MESSAGE}</a></p>
            </Card.Content>
        </Card>
    )
}

export default PresentationCard;
