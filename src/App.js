import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Container, Menu } from 'semantic-ui-react';
import './App.css';
import { faRocket } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import About from './components/About';
import Reservations from './components/Reservations';
import AdminPanel from './components/AdminPanel';
import Welcome from './components/Welcome';

function App() {
  return (
    <Router>
      <div>
        <Menu fixed='top' inverted>
          <Container>
            <Menu.Item className="navbar-logo">
              <FontAwesomeIcon icon={faRocket} size='2x' />
              <Link to="/">
                <label className="navbar-companyname"> Rocket Booking</label>
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link className='Nav-link Reservas' to='/Reservas'>
                Reservas
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link className='Nav-link PanelGestion' to='/PanelGestion'>
                Panel de Gestión
              </Link>
            </Menu.Item>
            <Menu.Item>
              <Link className='Nav-link Nosotros' to='/Nosotros'>
                Nosotros
              </Link>
            </Menu.Item>
          </Container>
        </Menu>
        <Switch>
          <Route path='/reservas'><Reservations /></Route>
          <Route path='/panelgestion'> <AdminPanel /> </Route>
          <Route path='/nosotros'> <About /> </Route>
          <Route exact path='/'> <Welcome /> </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
